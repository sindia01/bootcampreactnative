
// Soal 1
console.log(`\n- - - SOAL  1 - - -`);
function range (startNum, finishNum) {
    let rangeArray = [];

    if (startNum > finishNum){
        let rangeLength = startNum - finishNum + 1;
        for (let x = 0; x< rangeLength; x++){
            rangeArray.push(startNum - x)
        }
    }else if (startNum < finishNum){
        let rangeLength = finishNum - startNum + 1;
        for (let x = 0; x < rangeLength; x++){
            rangeArray.push(startNum + x)
        }
    }else if (!startNum || !finishNum){
        return -1
    }
    return rangeArray
}
console.log(`\n input = 1, 10`);
console.log(range(1, 10));
console.log(`\n input = 1`);
console.log(range(1));
console.log(`\n input = 11, 18`);
console.log(range(11, 18));
console.log(`\n input = 54, 50`);
console.log(range(54, 50));
console.log();



// SOAL 2
console.log(`\n- - - SOAL  2 - - -`);
function rangeWithStep (startNum, finishNum, step){
    let rangeArray = [];

    if (startNum > finishNum){
        let currentNum = startNum;
        for (let i = 0; currentNum >= finishNum; i++){
            rangeArray.push(currentNum)
            currentNum -= step
        }
    }else if (startNum < finishNum){
        let currentNum = startNum;
        for (let i = 0; currentNum <= finishNum; i++){
            rangeArray.push(currentNum)
            currentNum += step
        }
    }else if (!startNum || !finishNum || step){
        return -1
    }

    return rangeArray
}
console.log(`\n input = 1, 10, 2`);
console.log( rangeWithStep(1, 10, 2));
console.log(`\n input = 11,23,3`);
console.log(rangeWithStep(11, 23, 3));
console.log(`\n input = 5,2,1`);
console.log(rangeWithStep(5, 2, 1));
console.log(`\n input = 29,2,4`);
console.log(rangeWithStep(29, 2, 4)); 

// SOAL 3
console.log(`\n- - - SOAL  3 - - -`);
function sum(startNum, finishNum, step){
    let rangeArray = [];
    let gap;

    if(!step){
        gap=1
    }else {
        gap = step
    }
    if (startNum > finishNum){
        let currentNum = startNum;
        for (let i = 0; currentNum >= finishNum; i++){
            rangeArray.push(currentNum)
            currentNum -= gap
        }
    } else if (startNum < finishNum){
        let currentNum = startNum;
        for (let i = 0; currentNum <= finishNum; i++){
            rangeArray.push(currentNum)
            currentNum += gap
        }
    } else if (!startNum && !finishNum && !step){
        return 0
        
    } else if (startNum){
        return startNum
    }

    let total = 0;
    for (let i = 0; i < rangeArray.length; i++){
        total = total + rangeArray[i]
    }
    return total
}
console.log(`\n input = 1, 10`);
console.log(sum(1,10));
console.log(`\n input = 5, 50, 2`);
console.log(sum(5, 50, 2)); 
console.log(`\n input = 15, 10`);
console.log(sum(15,10));
console.log(`\n input = 20, 10, 2`);
console.log(sum(20, 10, 2)); 
console.log(`\n input = 1`);
console.log(sum(1));
console.log(`\n input = kosong`);
console.log(sum());

// SOAL 4
console.log(`\n- - - SOAL  4 - - -`);
let input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ] 

function dataHandling (data){
    let dataLength = data.length
        for (let x=0; x<dataLength; x++){
            let id = "\nNomor ID :" + data[x][0] 
            let nama = "Nama Lengkap :" + data[x][1] 
            let ttl = "TTL :" + data[x][2]+ " " + data[x][3] 
            let hobi = "Hobi :" + data[x][4]

        console.log(id);
        console.log(nama);
        console.log(ttl);
        console.log(hobi);

    }
}

dataHandling(input);


// SOAL 5
console.log(`\n- - - SOAL  5 - - -`);
function balikkata(kata1){
    let kata2 =' '
    for (let x = kata1.length-1; x >=0; x--){
        kata2 += kata1[x]
    }
    return kata2
}


console.log(`Kasur Rusak  > `+ balikkata("Kasur Rusak")); 
console.log(`SanberCode  >` + balikkata("SanberCode")); 
console.log(`Haji Ijah >` + balikkata("Haji Ijah")); 
console.log(`racecar >` + balikkata("racecar")); 
console.log(`I am Sanbers >` + balikkata("I am Sanbers")); 

// SOAL 6

console.log('\n- - - Soal 6 - - -')

var input1 = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input1);

function dataHandling2(data){
    var dataBaru = data
    var namaBaru = data[1] + ' Elsharawy'
    var provinsiBaru = 'Provinsi ' + data[2]
    var gender = 'Pria'
    var sekolah = 'SMA International Metro'

    dataBaru.splice(1, 1, namaBaru)
    dataBaru.splice(2, 1, provinsiBaru)
    dataBaru.splice(4, 1, gender, sekolah)

    var arrayTgl = data [3]
    var tglBaru = arrayTgl.split('/')
    var bulan = tglBaru[1]
    var namaBulan = ' '

    switch(bulan){
        case '01' :
            namaBulan = 'Januari'
            break;
        case '02' :
            namaBulan = 'Februari'
            break;
        case '03' :
            namaBulan = 'Maret'
            break;
        case '04' :
            namaBulan = 'April'
            break;
        case '05' :
            namaBulan = 'Mei'
            break;
        case '06' :
            namaBulan = 'Juni'
            break;
        case '07' :
            namaBulan = 'Juli'
            break;
        case '08' :
            namaBulan = 'Agustus'
            break;
        case '09' :
            namaBulan = 'September'
            break;
        case '10' :
            namaBulan = 'Oktober'
            break;
        case '11' :
            namaBulan = 'November'
            break;
        case '12' :
            namaBulan = 'Desember'
            break;

    }
    var tglJoin =  tglBaru.join(' ')
    var arrayTgl = tglBaru.sort(function(value1, value2){
        value2 - value1
    })
    var editNama = namaBaru.slice(0, 15)
    console.log(dataBaru)
    console.log(namaBulan)
    console.log(arrayTgl)
    console.log(tglJoin)
    console.log(editNama)
}

console.log(" ")
