// SOAL 1
console.log(`\n\n- - - S O A L  1 - - -\n`)

const golden  = () => {
    console.log(`this is golden!!`)
}
golden();

// SOAL 2
console.log(`\n\n- - - S O A L  2 - - -\n`)
const newFunction = (firstName,lastName) => {
    return{
        
        fullName(){
            console.log(`${firstName} ${lastName}`)
            
        }
    }
}
newFunction(`william`,`imoh`).fullName()

// SOAL 3
console.log(`\n\n- - - S O A L  3 - - -\n`)

const newObject = {
    firstName : `Harry`,
    lastName : `Potter Holt`,
    destination : `Hogwarts React Conf`,
    occupation : `Deve-wizard Avocado`,
    spell : `Vimulus Renderus!!!`
}
const {firstName, lastName, destination, occupation,spell} = newObject;
console.log(firstName, lastName, destination, occupation, spell)

// SOAL 4
console.log(`\n\n- - - S O A L  4 - - -\n`)

const west = [`Will`,`Christ`, `Sam`, `Holly`]
const east = [`Gill`, `Brian`,  `Noel`,`Maggie`]

const combined = [...west, ...east]
console.log(combined)

// SOAL 5
console.log(`\n\n- - - S O A L  5 - - -\n`)

const planet =  `earth`
const view = `glass`
let before = `Lorem ${view} dolor sit amet 
consectetur adipiscing elit ${planet} do eiusmod tempor
incididunt ut labore et dolore magna aliqua. Ut enim
ad minim veniam`

console.log(before)
console.log()