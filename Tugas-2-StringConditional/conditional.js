// Soal 1

var nama = "Jane";
var peran = "Penyihir";

if(nama == "Junaedi" || peran == "Werewolf"){
    console.log("Selamat Datang di Dunia Werewolf, Junaedi  \n Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!")
}else if(nama == "Jenita" || peran == "Guard"){
    console.log("Selamat datang di Dunia Werewolf, Jenita \n Halo Guard Jenita, Kamu akan membantu melindungi temanmu dari serangan werewolf.")
}else if(nama == "Jane" || peran == "Penyihir"){
    console.log("Selamat datang di Dunia Werewolf, Jane \n Halo penyihir Jane, Kamu dapat melihat siapa yang menjadi werewolf!")
}else if(nama == "John" || peran == " "){
    console.log("Halo John, Pilih peranmu untuk memulai Game!")
}else if(nama == " " || peran == " "){
    console.log("Nama Harus Diisi!!")
}


//Soal 2 Switch Case
var hari = 20;
var bulan = 2;
var tahun = 1999;

switch(bulan) {
    case 1:   { bulan = "Januari"; break; }
    case 2:   { bulan = "Februari"; break; }
    case 3:   { bulan = "Maret"; break; }
    case 4:   { bulan = "April"; break; }
    case 5:   { bulan = "Mei"; break; }
    case 6:   { bulan = "Juni"; break; }
    case 7:   { bulan = "Juli"; break; }
    case 8:   { bulan = "Agustus"; break; }
    case 9:   { bulan = "September"; break;}
    case 10:  { bulan = "Oktober"; break; }
    case 11:  { bulan = "November"; break; }
    case 12:  { bulan = "Desember"; break; }}

    console.log(`${hari} ${bulan} ${tahun}`);
