let readBooksPromise = require('./promise.js')
 
let books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 

async function asyncCall() {
    let time = 10000
    for (let x = 0; x < books.length; x++) {
        time = await readBooksPromise(time, books[x])
            .then((sisaWaktu) => {
                return sisaWaktu;
            })
            .catch((sisaWaktu) => {
                return sisaWaktu;
            });
    }
    console.log(`Selesai`)
}


asyncCall();
