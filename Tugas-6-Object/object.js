// SOAL 1

console.log(`\n- - - Soal 1 - - - \n`)
function arrayToObject(arr) {
    if (arr.length <= 0) {
        return console.log("")
    }
    for (let x = 0; x < arr.length; x++) {
        let newObject = {}
        let birthYear = arr[x][3];
        let now = new Date().getFullYear()
        let newAge;
        if (birthYear && now - birthYear > 0) {
            newAge = now - birthYear
        } else {
            newAge = "Invalid Birth Year"
        }
        newObject.firstName = arr[x][0]
        newObject.lastName = arr[x][1]
        newObject.gender = arr[x][2]
        newObject.age = newAge

        let consoleText = (x + 1) + ' .' + newObject.firstName + ' ' + newObject.lastName + ' : '

        console.log(consoleText)
        console.log(newObject)
    }
}

let people1 = [
    ["Bruce", "Banner", "male", 1975],
    ["Natasha", "Romanoff", "female"]
]
let people2 = [
    ["Tony", "Stark", "male", 1980], 
    ["Pepper", "Pots", "female", 2000],
    ["Scott", "Lang", "male", 2023]
    ]
arrayToObject(people2);
arrayToObject(people1);
arrayToObject([])



// SOAL 2
console.log(`\n- - - Soal 2 - - - \n`)
function shoppingTime(memberId, money) {
    if (!memberId) {
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    } else if (money < 50000) {
        return "Mohon Maaf, uang tidak cukup."
    } else {
        let newObject = {}
        let moneyChange = money;
        let purchaseList = [];
        let sepatuStacattu = "Sepatu Stacattu";
        let bajuZoro = "Baju Zoro";
        let bajuHn = "Baju H&N"
        let sweaterUniklooh = "Sweater Uniklooh";
        let casingHandphone = "Casing Handphone";

        let check = 0;
        for (x = 0; moneyChange >= 50000 && check == 0; x++) {
            if (moneyChange >= 1500000) {
                purchaseList.push(sepatuStacattu)
                moneyChange -= 1500000
            } else if (moneyChange >= 500000) {
                purchaseList.push(bajuZoro)
                moneyChange -= 500000
            } else if (moneyChange >= 250000) {
                purchaseList.push(bajuHn)
                moneyChange -= 250000
            } else if (moneyChange >= 175000) {
                purchaseList.push(sweaterUniklooh)
                moneyChange -= 175000
            } else if (moneyChange >= 50000) {
                for (let y = 0; y <= purchaseList.length - 1; y++) {
                    if (purchaseList[y] == casingHandphone) {
                        check += 1
                    }
                }
                if (check = 0) {
                    purchaseList.push(casingHandphone)
                    moneyChange -= 50000
                } else {
                    purchaseList.push(casingHandphone)
                    moneyChange -= 50000
                }
            }
        }
        newObject.memberId = memberId
        newObject.money = money
        newObject.listPurchase = purchaseList
        newObject.changeMoney = moneyChange
        return newObject
    }
}
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));

// SOAL 3
console.log(`\n- - - Soal 3 - - - \n`)
function naikAngkot(arrPenumpang) {
    let output = [];
    let rute = ['A', 'B', 'C', 'D', 'E', 'F'];

    for (let i = 0; i < arrPenumpang.length; i++) {
        let ongkos = 0;
        ongkos = (rute.indexOf(arrPenumpang[i][2]) - rute.indexOf(arrPenumpang[i][1])) * 2000;

        let angkot = {
            penumpang: arrPenumpang[i][0],
            naikDari: arrPenumpang[i][1],
            tujuan: arrPenumpang[i][2],
            bayar: ongkos
        }
        output.push(angkot);
    }
    return output;
}
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
